const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    alias: {
      src: path.resolve(__dirname, 'src/'),
      Templates: path.resolve(__dirname, 'src/templates/')
    }
  }
};