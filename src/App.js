import React, { useState } from 'react';
import Layout from './layout'
import TreeItem from './components/TreeItem/TreeItem'
import Button from './components/Button'
import Link from './components/Link'
import Modal from './components/Modal'

import './App.scss';
import './styles/index.scss';
function App() {

  const [show, setShow] = useState(false)
  const closeModal = () => {
    setShow(false)
  }
  const openModal = () => {
    setShow(true)
  }

  return (
    <div className="App App-container">
      <Layout>
        <h1>i18n file generator</h1>
        <TreeItem>
          16px item
        </TreeItem>
        <br />
        <TreeItem>
          16px item
        </TreeItem>
        <br />
        <Button click={openModal}>Add</Button>
        <br />
        <Link>Add</Link>
        <Modal show={show} onClose={closeModal}>
          <h1>SDFOIHASDFUH</h1>
        </Modal>
      </Layout>
    </div>
  );
}

export default App;
