import React from 'react'
import { CSSTransition } from 'react-transition-group'
import Close from '../../assets/Close.svg'
import './Modal.scss'

export default function Modal({ onClose, show, children }) {
  return (
    <div className={ `modal__container ${ show  ? 'modal--open' : '' }` }>
      <div className="modal__overlay" onClick={onClose} />
      <CSSTransition in={show} timeout={200} classNames="fade">
        <section className="modal__content">
          <img src={ Close } alt="close" className="modal__close" onClick={onClose} />
          { children }
        </section>
      </CSSTransition>
    </div>
  )
}
