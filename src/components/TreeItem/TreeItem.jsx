import React, { Component } from 'react'
import Trash from '../../assets/Trash.svg';
import Pen from '../../assets/Pen.svg';
import Plus from '../../assets/Plus.svg';
import Minus from '../../assets/Minus.svg';

import './index.scss'

export default class TreeItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      icon: Plus
    }
  }

  toggle () {
    this.setState({
      open: !this.state.open,
      icon: !this.state.open ? Minus : Plus
    })
  }

  render() {
    return (
      <div className={ `tree__item ${ this.state.open ? 'tree__item--selected' : '' }` }>
        <img className="tree__action--plus" src={ this.state.icon } alt="Plus" onClick={ this.toggle.bind(this) } />
        <p className="tree__text text-m">{ this.props.children }</p>
        <div className="tree--actions">
          <img className="tree__action tree__action--edit" src={ Pen } alt="Pen" />
          <img className="tree__action tree__action--delete" src={ Trash } alt="Trash" />
        </div>
      </div>
    )
  }
}
