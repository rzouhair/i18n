import React from 'react'
import './Button.scss'

function Button(props) {
  return (
    <button className="button__container" onClick={props.click} >
      { props.children }
    </button>
  )
}

export default Button

