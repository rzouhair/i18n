import React from 'react'
import './Link.scss'

function Link(props) {
  return (
    <p className="base__link" onClick={props.click}>{ props.children }</p>
  )
}

Link.propTypes = {

}

export default Link

