import React, { Component } from 'react'
import logo from '../assets/Logo.svg'

export default class Layout extends Component {
  render() {
    return (
      <div className="layout__container">
        <div className="layout__nav">
          <img src={ logo } alt="logo" />
        </div>
        <div className="layout__content">
          { this.props.children }
        </div>
      </div>
    )
  }
}
